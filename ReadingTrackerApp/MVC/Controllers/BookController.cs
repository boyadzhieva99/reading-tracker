﻿namespace MVC.Controllers
{
    using MVC.ViewModels;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    public class BookController : Controller
    {
        private const string JSON_MEDIA_TYPE = "application/json";
        private readonly string booksUri = "https://localhost:44391/api/Book";
        private readonly string genresUri = "https://localhost:44391/api/Genre";
        private readonly string publishersUri = "https://localhost:44391/api/Publisher";
        private readonly string authorsUri = "https://localhost:44391/api/Author";
        private readonly string bookAuthorsUri = "https://localhost:44391/api/BookAuthors";
        private static int bookId = 0;

        // GET: Books
        public async Task<ActionResult> Index()
        {
            List<BookVM> books = new List<BookVM>();

            using (var client = new HttpClient())
            {
                var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");

                if (token == null)
                {
                    return RedirectToAction("Login", "Account");
                }

                string tokenString = token.Value;

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                HttpResponseMessage response = await client.GetAsync(booksUri);

                if (response.IsSuccessStatusCode && token != null)
                {
                    //storing the response details received from the web api
                    var booksResponse = response.Content.ReadAsStringAsync().Result;
                    //deserializing the response and storing it into the list
                    books = JsonConvert.DeserializeObject<List<BookVM>>(booksResponse);
                }
            }
            return View(books);
        }

        // GET: Book/Details/5
        public async Task<ActionResult> Details(int id)
        {
            List<AuthorVM> bookAuthors = new List<AuthorVM>();
            using (var client = new HttpClient())
            {
                var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                string tokenString = token.Value;

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                HttpResponseMessage response = await client.GetAsync($"{bookAuthorsUri}/{id}");

                if (!response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

                var authorsResponse = response.Content.ReadAsStringAsync().Result;

                bookAuthors = JsonConvert.DeserializeObject<List<AuthorVM>>(authorsResponse);
            }
            return View(bookAuthors);
        }

        [HttpGet]
        public async Task<ActionResult> AddAuthor(int id)
        {
            bookId = id;
            using (var client = new HttpClient())
            {
                var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                string tokenString = token.Value;

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                ViewBag.BookAuthors = await GetAuthorsDropdownItems();
            }
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddAuthor(AuthorVM authorVM)
        {
            BookAuthorsVM bookAuthorsVM = new BookAuthorsVM();
            bookAuthorsVM.BookId = bookId;
            bookAuthorsVM.AuthorId = authorVM.Id;

            using (var client = new HttpClient())
            {
                var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                string tokenString = token.Value;

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                var serializedContent = JsonConvert.SerializeObject(bookAuthorsVM);
                var stringContent = new StringContent(serializedContent, Encoding.UTF8, JSON_MEDIA_TYPE);

                HttpResponseMessage response = await client.PostAsync(bookAuthorsUri, stringContent);

                if (!response.IsSuccessStatusCode || token == null)
                {
                    return View(authorVM);
                }
            }
            return RedirectToAction("Details", new { id = bookId });
        }

        // GET: Book/Create
        [HttpGet]
        public async Task<ActionResult> Create()
        {
            ViewBag.Genres = await GetGenresDropdownItems();
            ViewBag.Publishers = await GetPublishersDropdownItems();
            return View();
        }

        // POST: Book/Create
        [HttpPost]
        public async Task<ActionResult> Create(BookVM book)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                    string tokenString = token.Value;

                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                    var serializedContent = JsonConvert.SerializeObject(book);
                    var stringContent = new StringContent(serializedContent, Encoding.UTF8, JSON_MEDIA_TYPE);

                    HttpResponseMessage response = await client.PostAsync(booksUri, stringContent);

                    if (!response.IsSuccessStatusCode || token == null)
                    {
                        return View(book);
                    }
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View(book);
            }
        }

        // GET: Book/Edit/5
        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            using (var client = new HttpClient())
            {
                var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                string tokenString = token.Value;

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                HttpResponseMessage response = await client.GetAsync($"{booksUri}/{id}");

                if (!response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

                string jsonResponse = await response.Content.ReadAsStringAsync();

                var responseData = JsonConvert.DeserializeObject<BookVM>(jsonResponse);

                ViewBag.Genres = await GetGenresDropdownItems();
                ViewBag.Publishers = await GetPublishersDropdownItems();

                return View(responseData);
            }
        }

        // POST: Publisher/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, BookVM book)
        {
            book.Id = id;
            try
            {
                using (var client = new HttpClient())
                {
                    var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                    string tokenString = token.Value;

                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                    var serializedContent = JsonConvert.SerializeObject(book);
                    var stringContent = new StringContent(serializedContent, Encoding.UTF8, JSON_MEDIA_TYPE);

                    HttpResponseMessage response = await client.PutAsync($"{booksUri}/{id}", stringContent);

                    if (!response.IsSuccessStatusCode || token == null)
                    {
                        return View(book);
                    }
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Book/Delete/5
        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {
            using (var client = new HttpClient())
            {
                var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                string tokenString = token.Value;

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                HttpResponseMessage response = await client.GetAsync($"{booksUri}/{id}");

                if (!response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

                string jsonResponse = await response.Content.ReadAsStringAsync();

                var responseData = JsonConvert.DeserializeObject<BookVM>(jsonResponse);

                return View(responseData);
            }
        }

        // POST: Publisher/Delete/5
        [HttpPost, ActionName("Delete")]
        public async Task<ActionResult> Delete(int id, BookVM book)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                    string tokenString = token.Value;

                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                    HttpResponseMessage response = await client.DeleteAsync($"{booksUri}/{id}");

                    if (!response.IsSuccessStatusCode)
                    {
                        return View(book);
                    }
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View(book);
            }
        }

        //[HttpGet]
        //public async Task<ActionResult> AddBookToUser()
        //{
        //    using (HttpClient client = new HttpClient())
        //    {
        //        var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
        //        string tokenString = token.Value;

        //        client.DefaultRequestHeaders.Clear();
        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

        //        //HttpResponseMessage response = await client.GetAsync($"{userBooksUri}/{id}");

        //        if (!response.IsSuccessStatusCode)
        //        {
        //            return RedirectToAction("Index");
        //        }

        //        string jsonResponse = await response.Content.ReadAsStringAsync();

        //        var responseData = JsonConvert.DeserializeObject<BookVM>(jsonResponse);

        //        ViewBag.Genres = await GetGenresDropdownItems();
        //        ViewBag.Publishers = await GetPublishersDropdownItems();

        //        return View(responseData);
        //    }
        //}

        private async Task<IEnumerable<SelectListItem>> GetGenresDropdownItems()
        {
            //List<GenreVM> genresList = new List<GenreVM>();

            using (var client = new HttpClient())
            {
                var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                string tokenString = token.Value;

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                HttpResponseMessage genresResponse = await client.GetAsync(genresUri);

                if (!genresResponse.IsSuccessStatusCode)
                {
                    return Enumerable.Empty<SelectListItem>();
                }

                string genresJsonResponse = await genresResponse.Content.ReadAsStringAsync();

                var genres = JsonConvert.DeserializeObject<IEnumerable<GenreVM>>(genresJsonResponse);

                List<SelectListItem> genresList = new List<SelectListItem>();

                foreach (var x in genres)
                {
                    genresList.Add(new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                }
                return genresList;
            }
        }

        private async Task<IEnumerable<SelectListItem>> GetPublishersDropdownItems()
        {
            using (var client = new HttpClient())
            {
                var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                string tokenString = token.Value;

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                HttpResponseMessage publishersResponse = await client.GetAsync(publishersUri);

                if (!publishersResponse.IsSuccessStatusCode)
                {
                    return Enumerable.Empty<SelectListItem>();
                }

                string genresJsonResponse = await publishersResponse.Content.ReadAsStringAsync();

                var publishers = JsonConvert.DeserializeObject<IEnumerable<PublisherVM>>(genresJsonResponse);

                List<SelectListItem> publishersList = new List<SelectListItem>();

                foreach (var x in publishers)
                {
                    publishersList.Add(new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                }
                return publishersList;
            }
        }

        private async Task<IEnumerable<SelectListItem>> GetAuthorsDropdownItems()
        {
            using (var client = new HttpClient())
            {
                var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                string tokenString = token.Value;

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                HttpResponseMessage response = await client.GetAsync(authorsUri);

                if (!response.IsSuccessStatusCode)
                {
                    return Enumerable.Empty<SelectListItem>();
                }

                string authorsJsonResponse = await response.Content.ReadAsStringAsync();

                var authors = JsonConvert.DeserializeObject<IEnumerable<AuthorVM>>(authorsJsonResponse);

                List<SelectListItem> authorsList = new List<SelectListItem>();

                foreach (var x in authors)
                {
                    authorsList.Add(new SelectListItem()
                    {
                        Text = x.FName + " " + x.LName,
                        Value = x.Id.ToString()
                    });
                }
                return authorsList;
            }
        }

        //[HttpGet]
        //public ActionResult SearchBooksByTitle()
        //{
        //    using (var client = new HttpClient())
        //    {
        //        var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
        //        string tokenString = token.Value;

        //        client.DefaultRequestHeaders.Clear();
        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

        //        //HttpResponseMessage response = await client.GetAsync($"{booksUri}/{title}");

        //        return View();
        //    }
        //}

        //[HttpPost]
        //public async Task<ActionResult> SearchBooksByTitle(BookSearchVM searchVM)
        //{
        //    List<BookVM> books = new List<BookVM>();
        //    using (var client = new HttpClient())
        //    {
        //        var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
        //        string tokenString = token.Value;

        //        client.DefaultRequestHeaders.Clear();
        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

        //        HttpResponseMessage response = await client.GetAsync($"{booksUri}?title={searchVM.Title}");

        //        var booksResponse = response.Content.ReadAsStringAsync().Result;

        //        books = JsonConvert.DeserializeObject<List<BookVM>>(booksResponse);
        //    }
        //    return View(books);
        //}
    }
}