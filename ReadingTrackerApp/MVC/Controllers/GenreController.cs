﻿using ApplicationData.Entities;
using MVC.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class GenreController : Controller
    {
        private const string JSON_MEDIA_TYPE = "application/json";
        private readonly string baseUrl = "https://localhost:44391/api/Genre";

        // GET: Genre
        public async Task<ActionResult> Index()
        {
            List<GenreVM> genres = new List<GenreVM>();

            using (var client = new HttpClient())
            {
                var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");

                if (token == null)
                {
                    return RedirectToAction("Login", "Account");
                }

                string tokenString = token.Value;

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                HttpResponseMessage response = await client.GetAsync(baseUrl);

                if (response.IsSuccessStatusCode && token != null)
                {
                    //storing the response details received from the web api
                    var genreResponse = response.Content.ReadAsStringAsync().Result;
                    //deserializing the response and storing it into the list
                    genres = JsonConvert.DeserializeObject<List<GenreVM>>(genreResponse);
                }
            }
            return View(genres);
        }

        // GET: Genre/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Genre/Create
        [HttpPost]
        public async Task<ActionResult> Create(GenreVM genre)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                    string tokenString = token.Value;

                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                    var serializedContent = JsonConvert.SerializeObject(genre);
                    var stringContent = new StringContent(serializedContent, Encoding.UTF8, JSON_MEDIA_TYPE);

                    HttpResponseMessage response = await client.PostAsync(baseUrl, stringContent);

                    if (!response.IsSuccessStatusCode || token == null)
                    {
                        return View(genre);
                    }
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View(genre);
            }
        }

        // GET: Genre/Edit/5
        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            using (var client = new HttpClient())
            {
                var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                string tokenString = token.Value;

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                HttpResponseMessage response = await client.GetAsync($"{baseUrl}/{id}");

                if (!response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

                string jsonResponse = await response.Content.ReadAsStringAsync();

                var responseData = JsonConvert.DeserializeObject<GenreVM>(jsonResponse);

                return View(responseData);
            }
        }

        // POST: Genre/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, GenreVM genre)
        {
            genre.Id = id;
            try
            {
                using (var client = new HttpClient())
                {
                    var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                    string tokenString = token.Value;

                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                    var serializedContent = JsonConvert.SerializeObject(genre);
                    var stringContent = new StringContent(serializedContent, Encoding.UTF8, JSON_MEDIA_TYPE);

                    HttpResponseMessage response = await client.PutAsync($"{baseUrl}/{id}", stringContent);

                    if (!response.IsSuccessStatusCode || token == null)
                    {
                        return View(genre);
                    }
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Genre/Delete/5
        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {
            using (var client = new HttpClient())
            {
                var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                string tokenString = token.Value;

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                HttpResponseMessage response = await client.GetAsync($"{baseUrl}/{id}");

                if (!response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

                string jsonResponse = await response.Content.ReadAsStringAsync();

                var responseData = JsonConvert.DeserializeObject<GenreVM>(jsonResponse);

                return View(responseData);
            }
        }

        // POST: Genre/Delete/5
        [HttpPost, ActionName("Delete")]
        public async Task<ActionResult> Delete(int id, GenreVM genre)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                    string tokenString = token.Value;

                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                    HttpResponseMessage response = await client.DeleteAsync($"{baseUrl}/{id}");

                    if (!response.IsSuccessStatusCode)
                    {
                        return View(genre);
                    }
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View(genre);
            }
        }
    }
}
