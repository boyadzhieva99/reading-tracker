﻿using MVC.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class PublisherController : Controller
    {
        private const string JSON_MEDIA_TYPE = "application/json";
        private readonly string baseUrl = "https://localhost:44391/api/Publisher";

        // GET: Publisher
        public async Task<ActionResult> Index()
        {
            List<PublisherVM> publishers = new List<PublisherVM>();

            using (var client = new HttpClient())
            {
                var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");

                if(token == null)
                {
                    return RedirectToAction("Login", "Account");
                }

                string tokenString = token.Value;

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                HttpResponseMessage response = await client.GetAsync(baseUrl);

                if (response.IsSuccessStatusCode && token != null)
                {
                    //storing the response details received from the web api
                    var publishersResponse = response.Content.ReadAsStringAsync().Result;
                    //deserializing the response and storing it into the list
                    publishers = JsonConvert.DeserializeObject<List<PublisherVM>>(publishersResponse);
                }
            }
            return View(publishers);
        }

        // GET: Publisher/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Publisher/Create
        [HttpPost]
        public async Task<ActionResult> Create(PublisherVM publisher)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                    string tokenString = token.Value;

                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                    var serializedContent = JsonConvert.SerializeObject(publisher);
                    var stringContent = new StringContent(serializedContent, Encoding.UTF8, JSON_MEDIA_TYPE);

                    HttpResponseMessage response = await client.PostAsync(baseUrl, stringContent);

                    if (!response.IsSuccessStatusCode || token == null)
                    {
                        return View(publisher);
                    }
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View(publisher);
            }
        }

        // GET: Publisher/Edit/5
        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            using (var client = new HttpClient())
            {
                var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                string tokenString = token.Value;

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                HttpResponseMessage response = await client.GetAsync($"{baseUrl}/{id}");

                if (!response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

                string jsonResponse = await response.Content.ReadAsStringAsync();

                var responseData = JsonConvert.DeserializeObject<PublisherVM>(jsonResponse);

                return View(responseData);
            }
        }

        // POST: Publisher/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, PublisherVM publisher)
        {
            publisher.Id = id;
            try
            {
                using (var client = new HttpClient())
                {
                    var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                    string tokenString = token.Value;

                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                    var serializedContent = JsonConvert.SerializeObject(publisher);
                    var stringContent = new StringContent(serializedContent, Encoding.UTF8, JSON_MEDIA_TYPE);

                    HttpResponseMessage response = await client.PutAsync($"{baseUrl}/{id}", stringContent);

                    if (!response.IsSuccessStatusCode || token == null)
                    {
                        return View(publisher);
                    }
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Publisher/Delete/5
        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {
            using (var client = new HttpClient())
            {
                var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                string tokenString = token.Value;

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                HttpResponseMessage response = await client.GetAsync($"{baseUrl}/{id}");

                if (!response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

                string jsonResponse = await response.Content.ReadAsStringAsync();

                var responseData = JsonConvert.DeserializeObject<PublisherVM>(jsonResponse);

                return View(responseData);
            }
        }

        // POST: Publisher/Delete/5
        [HttpPost, ActionName("Delete")]
        public async Task<ActionResult> Delete(int id, PublisherVM publisher)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                    string tokenString = token.Value;

                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                    HttpResponseMessage response = await client.DeleteAsync($"{baseUrl}/{id}");

                    if (!response.IsSuccessStatusCode)
                    {
                        return View(publisher);
                    }
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View(publisher);
            }
        }
    }
}
