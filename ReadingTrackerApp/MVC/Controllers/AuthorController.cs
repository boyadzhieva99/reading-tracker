﻿namespace MVC.Controllers
{
    using MVC.ViewModels;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;

    public class AuthorController : Controller
    {
        private const string JSON_MEDIA_TYPE = "application/json";
        private readonly string baseUrl = "https://localhost:44391/api/Author";

        // GET: Author
        public async Task<ActionResult> Index()
        {
            List<AuthorVM> authors = new List<AuthorVM>();

            using (var client = new HttpClient())
            {
                var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");

                if (token == null)
                {
                    return RedirectToAction("Login", "Account");
                }

                string tokenString = token.Value;

                client.DefaultRequestHeaders.Clear();
                //client.DefaultRequestHeaders.Add("Authorization", "Bearer " + tokenString);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                HttpResponseMessage response = await client.GetAsync(baseUrl);

                if (response.IsSuccessStatusCode && token != null)
                {
                    //storing the response details received from the web api
                    var authorsResponse = response.Content.ReadAsStringAsync().Result;
                    //deserializing the response and storing it into the list
                    authors = JsonConvert.DeserializeObject<List<AuthorVM>>(authorsResponse);
                }
            }
            return View(authors);
        }

        // GET: Author/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Author/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Author/Create
        [HttpPost]
        public async Task<ActionResult> Create(AuthorVM author)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                    string tokenString = token.Value;

                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                    var serializedContent = JsonConvert.SerializeObject(author);
                    var stringContent = new StringContent(serializedContent, Encoding.UTF8, JSON_MEDIA_TYPE);

                    HttpResponseMessage response = await client.PostAsync(baseUrl, stringContent);

                    if (!response.IsSuccessStatusCode || token == null)
                    {
                        return View(author);
                    }
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View(author);
            }
        }

        // GET: Author/Edit/5
        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            using (var client = new HttpClient())
            {
                var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                string tokenString = token.Value;

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                HttpResponseMessage response = await client.GetAsync($"{baseUrl}/{id}");

                if (!response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

                string jsonResponse = await response.Content.ReadAsStringAsync();

                var responseData = JsonConvert.DeserializeObject<AuthorVM>(jsonResponse);

                return View(responseData);
            }
        }

        // POST: Author/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, AuthorVM author)
        {
            author.Id = id;
            try
            {
                using (var client = new HttpClient())
                {
                    var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                    string tokenString = token.Value;

                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                    var serializedContent = JsonConvert.SerializeObject(author);
                    var stringContent = new StringContent(serializedContent, Encoding.UTF8, JSON_MEDIA_TYPE);

                    HttpResponseMessage response = await client.PutAsync($"{baseUrl}/{id}", stringContent);

                    if (!response.IsSuccessStatusCode || token == null)
                    {
                        return View(author);
                    }
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Author/Delete/5
        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {
            using (var client = new HttpClient())
            {
                var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                string tokenString = token.Value;

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                HttpResponseMessage response = await client.GetAsync($"{baseUrl}/{id}");

                if (!response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

                string jsonResponse = await response.Content.ReadAsStringAsync();

                var responseData = JsonConvert.DeserializeObject<AuthorVM>(jsonResponse);

                return View(responseData);
            }
        }

        // POST: Author/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, AuthorVM author)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                    string tokenString = token.Value;

                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                    HttpResponseMessage response = await client.DeleteAsync($"{baseUrl}/{id}");

                    if (!response.IsSuccessStatusCode)
                    {
                        return View(author);
                    }
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View(author);
            }
        }
    }
}
