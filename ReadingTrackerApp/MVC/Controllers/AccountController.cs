﻿namespace MVC.Controllers
{
    using MVC.ViewModels;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    public class AccountController : Controller
    {
        private const string JSON_MEDIA_TYPE = "application/json";
        private readonly string baseUrl = "https://localhost:44391/";
        private readonly string getUserIdUri = "https://localhost:44391/api/Account/getId";
        HttpCookie accessTokenCookie = new HttpCookie("AccessTokenCookie");
        private static string currentUserEmail = "";
        private static string currentUserId = "";
        private static int bookId = 0;

        // GET: Account
        [HttpGet]
        public ActionResult Register()
        {
            Response.Cookies.Clear();
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterVM model)
        {
            Session["loggedUser"] = currentUserEmail;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl + "api/Account/Register");

                var postRegister = client.PostAsJsonAsync<RegisterVM>("Register", model);
                postRegister.Wait();

                var result = postRegister.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Login");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error!");
            return View(model);
        }

        [HttpGet]
        public ActionResult Login()
        {
            Response.Cookies.Clear();
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Login(LoginVM model)
        {
            using (var client = new HttpClient())
            {
                Response.Cookies.Clear();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                var formContent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", model.Email),
                    new KeyValuePair<string, string>("password", model.Password),
                });
                //send request
                HttpResponseMessage responseMessage = client.PostAsync("https://localhost:44391/Token", formContent).Result;

                var responseJson = await responseMessage.Content.ReadAsStringAsync();
                var jObject = JObject.Parse(responseJson);
                var token = jObject.GetValue("access_token").ToString();

                accessTokenCookie.Value = token;
                HttpContext.Response.Cookies.Add(accessTokenCookie);

                if (responseMessage.IsSuccessStatusCode && token != null)
                {
                    currentUserEmail = model.Email;
                    var serializedContent = JsonConvert.SerializeObject(currentUserEmail);
                    var stringContent = new StringContent(serializedContent, Encoding.UTF8, JSON_MEDIA_TYPE);
                    
                    HttpResponseMessage idResponseMessage = client.PostAsync(getUserIdUri, stringContent).Result;

                    var idResponseJson = await idResponseMessage.Content.ReadAsStringAsync();
                    currentUserId = idResponseJson;
                    return RedirectToAction("Index", "Home");
                }
            }
            ModelState.AddModelError(string.Empty, "Server Error!");
            return View(model);
        }

        [HttpGet]
        public ActionResult Logout()
        {
            accessTokenCookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Clear();
            Session["loggedUser"] = null;
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public async Task<ActionResult> GetUserBooks()
        {
            using (var client = new HttpClient())
            {
                var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                string tokenString = token.Value;

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

                var serializedContent = JsonConvert.SerializeObject(currentUserEmail);
                var stringContent = new StringContent(serializedContent, Encoding.UTF8, JSON_MEDIA_TYPE);

                HttpResponseMessage responseMessage = client.PostAsync(getUserIdUri, stringContent).Result;

                var responseJson = await responseMessage.Content.ReadAsStringAsync();
                //var jObject = JObject.Parse(responseJson);
                var currentUserId = responseJson;
            }
            return View();
        }

        [HttpGet]
        public ActionResult AddBookToUser(int id)
        {
            bookId = id;

            using (var client = new HttpClient())
            {
                var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
                string tokenString = token.Value;

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));
            }
            return View();
        }

        //[HttpPost]
        //public async Task<ActionResult> AddAuthor(AuthorVM authorVM)
        //{
        //    BookAuthorsVM bookAuthorsVM = new BookAuthorsVM();
        //    //bookAuthorsVM.BookId = bookId;
        //    bookAuthorsVM.AuthorId = authorVM.Id;

        //    using (var client = new HttpClient())
        //    {
        //        var token = HttpContext.Request.Cookies.Get("AccessTokenCookie");
        //        string tokenString = token.Value;

        //        client.DefaultRequestHeaders.Clear();
        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenString);
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));

        //        var serializedContent = JsonConvert.SerializeObject(bookAuthorsVM);
        //        var stringContent = new StringContent(serializedContent, Encoding.UTF8, JSON_MEDIA_TYPE);

        //        //HttpResponseMessage response = await client.PostAsync(bookAuthorsUri, stringContent);

        //        if (!response.IsSuccessStatusCode || token == null)
        //        {
        //            return View(authorVM);
        //        }
        //    }
        //    return RedirectToAction("Details", new { id = bookId });
        //}

        //private Task<dynamic> GetBooksDropdownItems()
        //{
        //    throw new NotImplementedException();
        //}
    }
}