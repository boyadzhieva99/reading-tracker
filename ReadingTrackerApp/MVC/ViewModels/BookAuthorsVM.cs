﻿using System.ComponentModel;

namespace MVC.ViewModels
{
    public class BookAuthorsVM
    {
        public int BookId { get; set; }

        [DisplayName("Author")]
        public int AuthorId { get; set; }
    }
}