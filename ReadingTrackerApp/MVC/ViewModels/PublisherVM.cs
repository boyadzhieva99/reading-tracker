﻿namespace MVC.ViewModels
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public class PublisherVM
    {
        public int Id { get; set; }

        [Required]
        [StringLength(25, ErrorMessage = "Name cannot be longer than 25 letters!")]
        public string Name { get; set; }

        [StringLength(50, ErrorMessage = "CEO name cannot be longer than 30 letters!")]
        public string CEO { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Country name cannot be longer than 50 letters!")]
        public string Country { get; set; }

        [StringLength(100, ErrorMessage = "Address cannot be longer than 100 symbols!")]
        public string Address { get; set; }

        [DisplayName("Established Since")]
        public byte YearsActive { get; set; }
    }
}