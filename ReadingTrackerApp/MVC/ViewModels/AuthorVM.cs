﻿namespace MVC.ViewModels
{
    using ApplicationData.Entities;
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public class AuthorVM
    {
        [DisplayName("Author")]
        public int Id { get; set; }

        [Required]
        [DisplayName("First Name")]
        [StringLength(25, ErrorMessage = "First name cannot be longer than 25 letters!")]
        public string FName { get; set; }

        [Required]
        [DisplayName("Last Name")]
        [StringLength(25, ErrorMessage = "Last name cannot be longer than 25 letters!")]
        public string LName { get; set; }

        [DisplayName("Date of Birth")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMMM/yyyy}")]
        public DateTime? DateOfBirth { get; set; }

        [Range(0,100, ErrorMessage = "Age must be an integer value between 0 and 100!")]        
        public byte Age { get; set; }

        [Required]
        public Gender Gender { get; set; }

        [StringLength(50, ErrorMessage = "Nationality cannot contain more than 50 symbols!")]
        public string Nationality { get; set; }
    }
}