﻿namespace MVC.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    public class GenreVM
    {
        public int Id { get; set; }

        [Required]
        [StringLength(25, ErrorMessage = "Name cannot be longer than 25 letters!")]
        public string Name { get; set; }
    }
}