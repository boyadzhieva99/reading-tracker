﻿namespace MVC.ViewModels
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public class BookVM
    {
        public int Id { get; set; }

        [Required]
        [Range(0, long.MaxValue, ErrorMessage = "ISBN must be e positive number with either 9 or 13 digits!")]
        public long ISBN { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Title cannot be longer than 100 symbols!")]
        public string Title { get; set; }

        [DisplayName("Publication Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMMM/yyyy}")]
        public DateTime? PublicationDate { get; set; }

        [Required]
        [DisplayName("Genre")]
        public int GenreId { get; set; }
        public GenreVM Genre { get; set; }

        [Required]
        [DisplayName("Publisher")]
        public int PublisherId { get; set; }
        public PublisherVM Publisher { get; set; }

        public decimal Price { get; set; }

        [StringLength(6000)]
        public string Description { get; set; }

        [StringLength(2000)]
        [DisplayName("Cover")]
        public string ImageUrl { get; set; }
    }
}