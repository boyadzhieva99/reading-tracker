﻿namespace MVC.ViewModels
{
    using ApplicationData.Entities;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public class UserBooksVM
    {
        public string UserId { get; set; }
        public int BookId { get; set; }

        [DisplayName("Format")]
        public BookFormat BookFormat { get; set; }

        [DisplayName("Status")]
        public BookStaus BookStaus { get; set; }

        [DisplayName("Rating")]
        public BookRating BookRating { get; set; }

        [StringLength(6000, ErrorMessage = "Review cannot be more than 6000 characters")]
        public string Review { get; set; }
    }
}