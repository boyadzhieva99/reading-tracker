﻿namespace ApplicationServices.DTOs
{
    using ApplicationData.Entities;
    using System;

    public class AuthorDto : MainDto, IValidate
    {
        public string FName { get; set; }
        public string LName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public byte Age { get; set; }
        public Gender Gender { get; set; }
        public string Nationality { get; set; }

        public bool Validate()
        {
            if (FName.Length > 25 || LName.Length > 25 || Nationality.Length > 50)
            {
                return false;
            }
            return true;
        }
    }
}