﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.DTOs
{
    public class BookAuthorsDto
    {
        public int BookId { get; set; }
        public int AuthorId { get; set; }
    }
}
