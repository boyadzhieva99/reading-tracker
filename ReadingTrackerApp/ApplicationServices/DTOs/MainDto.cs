﻿using System;

namespace ApplicationServices.DTOs
{
    public class MainDto
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}