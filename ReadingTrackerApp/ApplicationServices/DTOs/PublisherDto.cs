﻿namespace ApplicationServices.DTOs
{
    using System;
    using System.Collections.Generic;

    public class PublisherDto : MainDto, IValidate
    {
        public string Name { get; set; }
        public string CEO { get; set; }
        public string Country { get; set; }
        //public char YearCreated { get; set; }
        public string Address { get; set; }
        public byte YearsActive { get; set; }

        public bool Validate()
        {
            if (Name.Length > 25 || CEO.Length > 50 || Country.Length > 50
                /*|| YearCreated.ToString().Length > 4*/ || Address.Length > 100)
            {
                return false;
            }
            return true;
        }
    }
}