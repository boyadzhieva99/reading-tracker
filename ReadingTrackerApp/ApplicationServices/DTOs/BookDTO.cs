﻿namespace ApplicationServices.DTOs
{
    using System;

    public class BookDto : MainDto, IValidate
    {
        public long ISBN { get; set; }
        public string Title { get; set; }
        public DateTime? PublicationDate { get; set; }
        public int GenreId { get; set; }
        public GenreDto Genre { get; set; }
        public int PublisherId { get; set; }
        public PublisherDto Publisher { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }

        public bool Validate()
        {
            if(ISBN < 0 || ISBN.ToString().Length > 13 || Title.Length > 50
                || Description.Length > 6000 || ImageUrl.Length > 2000)
            {
                return false;
            }
            return true;
        }
    }
}