﻿namespace ApplicationServices.DTOs
{
    public interface IValidate
    {
        bool Validate();
    }
}