﻿namespace ApplicationServices.DTOs
{
    using System;
    public class GenreDto : MainDto, IValidate
    {
        public string Name { get; set; }

        public bool Validate()
        {
            if (string.IsNullOrEmpty(Name) || Name.Length > 25)
            {
                return false;
            }
            return true;
        }
    }
}