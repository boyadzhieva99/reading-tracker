﻿namespace ApplicationServices.Services
{
    using ApplicationData.Context;
    using ApplicationData.Entities;
    using ApplicationData.Repository;
    using ApplicationServices.DTOs;
    using System;
    using System.Collections.Generic;

    public class PublisherManagementService
    {
        private ReadingTrackerDbContext context = new ReadingTrackerDbContext();

        public List<PublisherDto> Get()
        {
            List<PublisherDto> publisherDtos = new List<PublisherDto>();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                foreach (var item in unitOfWork.PublisherRepository.Get())
                {
                    publisherDtos.Add(new PublisherDto()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        CEO = item.CEO,
                        Country = item.Country,
                        //YearCreated = item.YearCreated,
                        Address = item.Address,
                        YearsActive = item.YearsActive
                    });
                }
            }
            return publisherDtos;
        }

        public PublisherDto GetById(int id)
        {
            PublisherDto publisherDto = new PublisherDto();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Publisher publisher = unitOfWork.PublisherRepository.GetById(id);
                if (publisher != null)
                {
                    publisherDto = new PublisherDto()
                    {
                        Id = publisher.Id,
                        Name = publisher.Name,
                        CEO = publisher.CEO,
                        Country = publisher.Country,
                        //YearCreated = publisher.YearCreated,
                        Address = publisher.Address,
                        YearsActive = publisher.YearsActive
                    };
                }
            }
            return publisherDto;
        }

        public bool Save(PublisherDto publisherDto)
        {
            Publisher publisher = new Publisher()
            {
                Name = publisherDto.Name,
                CEO = publisherDto.CEO,
                Country = publisherDto.Country,
                //YearCreated = publisherDto.YearCreated,
                Address = publisherDto.Address,
                YearsActive = publisherDto.YearsActive,
                CreatedOn = DateTime.Now
            };

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    unitOfWork.PublisherRepository.Insert(publisher);
                    unitOfWork.Save();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(PublisherDto publisherDto)
        {
            Publisher publisher = new Publisher()
            {
                Id = publisherDto.Id,
                Name = publisherDto.Name,
                CEO = publisherDto.CEO,
                Country = publisherDto.Country,
                //YearCreated = publisherDto.YearCreated,
                Address = publisherDto.Address,
                YearsActive = publisherDto.YearsActive,
                UpdatedOn = DateTime.Now
            };

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    unitOfWork.PublisherRepository.Update(publisher);
                    unitOfWork.Save();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Publisher publisher = unitOfWork.PublisherRepository.GetById(id);
                    unitOfWork.PublisherRepository.Delete(publisher);
                    unitOfWork.Save();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}