﻿namespace ApplicationServices.Services
{
    using ApplicationData.Context;
    using ApplicationData.Entities;
    using ApplicationData.Repository;
    using ApplicationServices.DTOs;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class BookManagementService
    {
        public IEnumerable<BookDto> GetByTitle(string title)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                var books = unitOfWork.BookRepository.Get(m => m.Title.Contains(title));

                var result = books.Select(book => new BookDto()
                {
                    Id = book.Id,
                    ISBN = book.ISBN,
                    Title = book.Title,
                    PublicationDate = book.PublicationDate,
                    Genre = new GenreDto()
                    {
                        Id = book.GenreId,
                        //Name = book.Genre.Name
                    },
                    Publisher = new PublisherDto()
                    {
                        Id = book.PublisherId,
                        //Name = book.Publisher.Name,
                        //CEO = book.Publisher.CEO,
                        //Country = book.Publisher.Country,
                        //Address = book.Publisher.Address,
                        //YearsActive = book.Publisher.YearsActive
                    },
                    Price = book.Price,
                    Description = book.Description,
                    ImageUrl = book.ImageUrl
                }).ToList();

                return result;
            }
        }

        public List<BookDto> Get()
        {
            List<BookDto> booksDtos = new List<BookDto>();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                foreach (var item in unitOfWork.BookRepository.Get())
                {
                    booksDtos.Add(new BookDto()
                    {
                        Id = item.Id,
                        ISBN = item.ISBN,
                        Title = item.Title,
                        PublicationDate = item.PublicationDate,                        
                        Genre = new GenreDto()
                        {
                            Id = item.GenreId,
                            //Name = item.Genre.Name
                        },
                        Publisher = new PublisherDto()
                        {
                            Id = item.PublisherId,
                            //Name = item.Publisher.Name,
                            //CEO = item.Publisher.CEO,
                            //Country = item.Publisher.Country,
                            //Address = item.Publisher.Address,
                            //YearsActive = item.Publisher.YearsActive
                        },
                        Price = item.Price,
                        Description = item.Description,
                        ImageUrl = item.ImageUrl
                    });
                }
            }
            return booksDtos;
        }

        public BookDto GetById(int id)
        {
            BookDto bookDto = new BookDto();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Book book = unitOfWork.BookRepository.GetById(id);

                if (book != null)
                {
                    bookDto = new BookDto()
                    {
                        Id = book.Id,
                        ISBN = book.ISBN,
                        Title = book.Title,
                        PublicationDate = book.PublicationDate,
                        Genre = new GenreDto()
                        {
                            Id = book.GenreId,
                            //Name = book.Genre.Name
                        },
                        Publisher = new PublisherDto()
                        {
                            Id = book.PublisherId,
                            //Name = book.Publisher.Name,
                            //CEO = book.Publisher.CEO,
                            //Country = book.Publisher.Country,
                            //Address = book.Publisher.Address,
                            //YearsActive = book.Publisher.YearsActive
                        },
                        Price = book.Price,
                        Description = book.Description,
                        ImageUrl = book.ImageUrl
                    };
                }
            }
            return bookDto;
        }

        public bool Save(BookDto bookDto)
        {
            if (bookDto.PublisherId == 0 || bookDto.GenreId == 0)
            {
                return false;
            }

            Book book = new Book()
            {
                ISBN = bookDto.ISBN,
                Title = bookDto.Title,
                PublicationDate = bookDto.PublicationDate,
                GenreId = bookDto.GenreId,
                PublisherId = bookDto.PublisherId,
                Price = bookDto.Price,
                Description = bookDto.Description,
                ImageUrl = bookDto.ImageUrl,
                CreatedOn = DateTime.Now
            };

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Genre genre = unitOfWork.GenreRepository.GetById(book.GenreId);
                    Publisher publisher = unitOfWork.PublisherRepository.GetById(book.PublisherId);
                    if (genre != null && publisher != null)
                    {
                        book.Genre = genre;
                        book.Publisher = publisher;
                        unitOfWork.BookRepository.Insert(book);
                        unitOfWork.Save();
                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }

        }

        public bool Update(BookDto bookDto)
        {
            if (bookDto.PublisherId == 0 || bookDto.GenreId == 0)
            {
                return false;
            }

            Book book = new Book()
            {
                Id = bookDto.Id,
                ISBN = bookDto.ISBN,
                Title = bookDto.Title,
                PublicationDate = bookDto.PublicationDate,
                GenreId = bookDto.GenreId,
                PublisherId = bookDto.PublisherId,
                Price = bookDto.Price,
                Description = bookDto.Description,
                ImageUrl = bookDto.ImageUrl,
                UpdatedOn = DateTime.Now
            };

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Genre genre = unitOfWork.GenreRepository.GetById(book.GenreId);
                    Publisher publisher = unitOfWork.PublisherRepository.GetById(book.PublisherId);
                    if (genre != null && publisher != null)
                    {
                        book.Genre = genre;
                        book.GenreId = bookDto.GenreId;
                        book.Publisher = publisher;
                        book.PublisherId = bookDto.PublisherId;
                        unitOfWork.BookRepository.Update(book);
                        unitOfWork.Save();
                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Book book = unitOfWork.BookRepository.GetById(id);
                    unitOfWork.BookRepository.Delete(book);
                    unitOfWork.Save();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }       
    }
}