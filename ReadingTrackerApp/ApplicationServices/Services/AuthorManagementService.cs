﻿namespace ApplicationServices.Services
{
    using ApplicationData.Context;
    using ApplicationData.Entities;
    using ApplicationData.Repository;
    using ApplicationServices.DTOs;
    using System;
    using System.Collections.Generic;

    public class AuthorManagementService
    {
        private ReadingTrackerDbContext context = new ReadingTrackerDbContext();

        public List<AuthorDto> Get()
        {
            List<AuthorDto> authorsDtos = new List<AuthorDto>();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                foreach (var item in unitOfWork.AuthorRepository.Get())
                {
                    authorsDtos.Add(new AuthorDto()
                    {
                        Id = item.Id,
                        FName = item.FName,
                        LName = item.LName,
                        DateOfBirth = item.DateOfBirth,
                        Age = item.Age,
                        Gender = item.Gender,
                        Nationality = item.Nationality
                    });
                }
            }
            return authorsDtos;
        }

        public AuthorDto GetById(int id)
        {
            AuthorDto authorDto = new AuthorDto();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Author author = unitOfWork.AuthorRepository.GetById(id);
                if (author != null)
                {
                    authorDto = new AuthorDto()
                    {
                        Id = author.Id,
                        FName = author.FName,
                        LName = author.LName,
                        DateOfBirth = author.DateOfBirth,
                        Age = author.Age,
                        Gender = author.Gender,
                        Nationality = author.Nationality
                    };
                }
            }
            return authorDto;
        }

        public bool Save(AuthorDto authorDto)
        {
            Author author = new Author()
            {
                FName = authorDto.FName,
                LName = authorDto.LName,
                DateOfBirth = authorDto.DateOfBirth,
                Age = authorDto.Age,
                Gender = authorDto.Gender,
                Nationality = authorDto.Nationality,
                CreatedOn = DateTime.Now
            };

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    unitOfWork.AuthorRepository.Insert(author);
                    unitOfWork.Save();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(AuthorDto authorDto)
        {
            Author author = new Author()
            {
                Id = authorDto.Id,
                FName = authorDto.FName,
                LName = authorDto.LName,
                DateOfBirth = authorDto.DateOfBirth,
                Age = authorDto.Age,
                Gender = authorDto.Gender,
                Nationality = authorDto.Nationality,
                UpdatedOn = DateTime.Now
            };

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    unitOfWork.AuthorRepository.Update(author);
                    unitOfWork.Save();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            Author author = new Author();

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    author = unitOfWork.AuthorRepository.GetById(id);
                    unitOfWork.AuthorRepository.Delete(author);
                    unitOfWork.Save();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}