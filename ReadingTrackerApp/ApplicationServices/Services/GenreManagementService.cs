﻿namespace ApplicationServices.Services
{
    using ApplicationData.Context;
    using ApplicationData.Entities;
    using ApplicationData.Repository;
    using ApplicationServices.DTOs;
    using System;
    using System.Collections.Generic;

    public class GenreManagementService
    {
        private ReadingTrackerDbContext context = new ReadingTrackerDbContext();

        public List<GenreDto> Get()
        {
            List<GenreDto> genresDtos = new List<GenreDto>();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                foreach (var item in unitOfWork.GenreRepository.Get())
                {
                    genresDtos.Add(new GenreDto()
                    {
                        Id = item.Id,
                        Name = item.Name
                    });
                }
            }
            return genresDtos;
        }

        public GenreDto GetById(int id)
        {
            GenreDto genreDto = new GenreDto();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Genre genre = unitOfWork.GenreRepository.GetById(id);

                if (genre != null)
                {
                    genreDto = new GenreDto()
                    {
                        Id = genre.Id,
                        Name = genre.Name
                    };
                }
            }
            return genreDto;
        }

        public bool Save(GenreDto genreDto)
        {
            Genre genre = new Genre()
            {
                Name = genreDto.Name,
                CreatedOn = DateTime.Now
            };

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    unitOfWork.GenreRepository.Insert(genre);
                    unitOfWork.Save();
                }
                return true;
            }
            catch
            {
                return false;
            }           

        }

        public bool Update(GenreDto genreDto)
        {
            Genre genre = new Genre()
            {
                Id = genreDto.Id,
                Name = genreDto.Name,
                UpdatedOn = DateTime.Now
            };

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    unitOfWork.GenreRepository.Update(genre);
                    unitOfWork.Save();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Genre genre = unitOfWork.GenreRepository.GetById(id);
                    unitOfWork.GenreRepository.Delete(genre);
                    unitOfWork.Save();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}