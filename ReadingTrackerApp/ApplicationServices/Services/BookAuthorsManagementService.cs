﻿namespace ApplicationServices.Services
{
    using ApplicationData.Entities;
    using ApplicationData.Repository;
    using ApplicationServices.DTOs;
    using System.Collections.Generic;

    public class BookAuthorsManagementService
    {
        //takes the id of the book
        public List<AuthorDto> GetAuthorsByBookId(int id)
        {
            List<AuthorDto> authors = new List<AuthorDto>();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Book book = unitOfWork.BookRepository.GetById(id);

                if (book != null && book.Authors != null)
                {
                    foreach (var item in book.Authors)
                    {
                        authors.Add(new AuthorDto() 
                        {
                            Id = item.Id,
                            FName = item.FName,
                            LName = item.LName,
                            DateOfBirth = item.DateOfBirth,
                            Age = item.Age,
                            Gender = item.Gender,
                            Nationality = item.Nationality
                        });
                    }
                }
            }
            return authors;
        }

        public bool AddAuthors(BookAuthorsDto dto)
        {
            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Book book = unitOfWork.BookRepository.GetById(dto.BookId);
                    Author author = unitOfWork.AuthorRepository.GetById(dto.AuthorId);

                    if (book != null && author != null)
                    {
                        book.Authors.Add(author);
                        unitOfWork.Save();
                        return true;
                    }
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
    }
}