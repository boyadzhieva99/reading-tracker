﻿namespace ApplicationData.Context
{
    using ApplicationData.Entities;
    using System.Data.Entity;
    public class ReadingTrackerDbContext : DbContext
    {
        public DbSet<Author> Authors { get; set; }
        public DbSet<Publisher> Publishers { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Book> Books { get; set; }

        public ReadingTrackerDbContext() 
            : base(@"Server=DESKTOP-J9GGGCT\SQLEXPRESS;Database=ReadingTrackerDB;Trusted_Connection=True;")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Book>()
                        .Property(p => p.Price)
                        .HasPrecision(5, 2);
        }
    }
}