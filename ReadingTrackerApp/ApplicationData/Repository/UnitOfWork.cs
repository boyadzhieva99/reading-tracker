﻿namespace ApplicationData.Repository
{
    using System;
    using ApplicationData.Context;
    using ApplicationData.Entities;

    public class UnitOfWork : IDisposable
    {
        private ReadingTrackerDbContext context = new ReadingTrackerDbContext();
        private MainRepository<Author> authorRepository;
        private MainRepository<Publisher> publisherRepository;
        private MainRepository<Genre> genreRepository;
        private MainRepository<Book> bookRepository;

        public MainRepository<Author> AuthorRepository 
        {
            get
            {
                if(this.authorRepository == null)
                {
                    this.authorRepository = new MainRepository<Author>(context);
                }
                return authorRepository;
            }
        }

        public MainRepository<Publisher> PublisherRepository
        {
            get
            {
                if (this.publisherRepository == null)
                {
                    this.publisherRepository = new MainRepository<Publisher>(context);
                }
                return publisherRepository;
            }
        }

        public MainRepository<Genre> GenreRepository
        {
            get
            {
                if (this.genreRepository == null)
                {
                    this.genreRepository = new MainRepository<Genre>(context);
                }
                return genreRepository;
            }
        }

        public MainRepository<Book> BookRepository
        {
            get
            {
                if (this.bookRepository == null)
                {
                    this.bookRepository = new MainRepository<Book>(context);
                }
                return bookRepository;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool isDisposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.isDisposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.isDisposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}