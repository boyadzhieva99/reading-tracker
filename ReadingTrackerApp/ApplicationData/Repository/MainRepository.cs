﻿namespace ApplicationData.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using ApplicationData.Context;
    using ApplicationData.Entities;

    public class MainRepository<E> where E : MainEntity
    {
        private ReadingTrackerDbContext context;
        private DbSet<E> dbSet;

        public MainRepository(ReadingTrackerDbContext context)
        {
            this.context = context;
            this.dbSet = context.Set<E>();
        }

        public virtual IEnumerable<E> Get(Expression<Func<E, bool>> filter = null,
                                          Func<IQueryable<E>, IOrderedQueryable<E>> orderBy = null,
                                          string includeProperties = "")
        {
            IQueryable<E> query = dbSet;

            if(filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if(orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual E GetById(object id)
        {
            return dbSet.Find(id);
        }

        public virtual void Insert(E entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            E entity = dbSet.Find(id);
            Delete(entity);
        }

        public virtual void Delete(E entity)
        {
            if(context.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            dbSet.Remove(entity);
        }

        public virtual void Update(E entity)
        {
            dbSet.Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }
    }
}