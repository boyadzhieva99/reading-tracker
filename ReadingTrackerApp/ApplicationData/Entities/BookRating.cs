﻿namespace ApplicationData.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Reflection;

    public enum BookRating
    {
        [Display(Name = "★")]
        OneStar = 1,
        [Display(Name = "★★")]
        TwoStars = 2,
        [Display(Name = "★★★")]
        ThreeStars = 3,
        [Display(Name = "★★★★")]
        FourStars = 4,
        [Display(Name = "★★★★★")]
        FiveStars = 5
    }

    public static class BookRatingEnumExtensions
    {
        public static string GetDisplayName(this BookRating enumValue)
        {
            return enumValue.GetType()
                            .GetMember(enumValue.ToString())
                            .First()
                            .GetCustomAttribute<DisplayAttribute>()
                            .GetName();
        }
    }
}
