﻿namespace ApplicationData.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    public class Genre : MainEntity
    {
        [Required]
        [StringLength(25)]
        //, ErrorMessage = "Name cannot be longer than 25 letters!"
        public string Name { get; set; }

        public ICollection<Book> Books { get; set; }
    }
}