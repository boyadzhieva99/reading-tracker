﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace ApplicationData.Entities
{
    public enum BookFormat
    {
        [Display(Name = "Audiobook")]
        Audiobook = 0,
        [Display(Name = "E-book")]
        Ebook = 1,
        [Display(Name = "Physical")]
        Physical = 2
    }

    public static class BookFormatEnumExtensions
    {
        public static string GetDisplayName(this BookFormat enumValue)
        {
            return enumValue.GetType()
                            .GetMember(enumValue.ToString())
                            .First()
                            .GetCustomAttribute<DisplayAttribute>()
                            .GetName();
        }
    }
}