﻿namespace ApplicationData.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Book : MainEntity
    {
        public Book()
        {
            this.Authors = new List<Author>();
        }

        [Required]
        public long ISBN { get; set; }

        [Required]
        [StringLength(100)]
        //, ErrorMessage = "Title cannot be longer than 100 symbols!"
        public string Title { get; set; }

        public DateTime? PublicationDate { get; set; }

        //[Required]
        //public int AuthorId { get; set; }
        //[ForeignKey("AuthorId")]
        //public Author Author { get; set; }

        [Required]
        public int GenreId { get; set; }
        [ForeignKey("GenreId")]
        public Genre Genre { get; set; }

        [Required]
        public int PublisherId { get; set; }
        [ForeignKey("PublisherId")]
        public Publisher Publisher { get; set; }

        public decimal Price { get; set; }

        [StringLength(6000)]
        public string Description { get; set; }

        [StringLength(2000)]
        public string ImageUrl { get; set; }

        public virtual ICollection<Author> Authors { get; set; }
    }
}