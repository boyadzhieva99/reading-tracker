﻿namespace ApplicationData.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Reflection;

    public enum Gender
    {
        [Display(Name = "Male")]
        Male = 0,
        [Display(Name = "Female")]
        Female = 1
    }

    public static class GenderEnumExtensions
    {
        public static string GetDisplayName(this Gender enumValue)
        {
            return enumValue.GetType()
                            .GetMember(enumValue.ToString())
                            .First()
                            .GetCustomAttribute<DisplayAttribute>()
                            .GetName();
        }
    }
}