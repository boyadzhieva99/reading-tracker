﻿namespace ApplicationData.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Author : MainEntity
    {
        public Author()
        {
            this.Books = new List<Book>();
        }

        [Required]
        [StringLength(25)]
        public string FName { get; set; }

        [Required]
        [StringLength(25)]
        public string LName { get; set; }

        public DateTime? DateOfBirth { get; set; }

        [Range(0,100)]
        public byte Age { get; set; }

        [Required]
        public Gender Gender { get; set; }

        [StringLength(50)]
        public string Nationality { get; set; }

        public virtual ICollection<Book> Books { get; set; }

    }
}