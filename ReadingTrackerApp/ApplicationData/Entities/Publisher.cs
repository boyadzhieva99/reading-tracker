﻿namespace ApplicationData.Entities
{
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Publisher : MainEntity
    {
        [Required]
        [StringLength(25)]
        //, ErrorMessage = "Name cannot be longer than 25 letters!"
        public string Name { get; set; }

        [StringLength(50)]
        //, ErrorMessage = "CEO name cannot be longer than 30 letters!"
        public string CEO { get; set; }

        [Required]
        [StringLength(50)]
        //, ErrorMessage = "Country name cannot be longer than 50 letters!"
        public string Country { get; set; }

        //[StringLength(4)]
        ////, ErrorMessage = "Enter only the year!"
        //public char YearCreated { get; set; }

        [StringLength(100)]
        //, ErrorMessage = "Address cannot be longer than 100 symbols!"
        public string Address { get; set; }

        // ErrorMessage = "Years count cannot be negative number!
        public byte YearsActive { get; set; }

        public ICollection<Book> Books { get; set; }

    }
}