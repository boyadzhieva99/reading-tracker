﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace ApplicationData.Entities
{
    public enum BookStaus
    {
        [Display(Name = "TBR")]
        TBR = 0,
        [Display(Name = "Currently Reading")]
        CurrentlyReading = 1,
        [Display(Name = "Read")]
        Read = 2
    }

    public static class BookStatusEnumExtensions
    {
        public static string GetDisplayName(this BookStaus enumValue)
        {
            return enumValue.GetType()
                            .GetMember(enumValue.ToString())
                            .First()
                            .GetCustomAttribute<DisplayAttribute>()
                            .GetName();
        }
    }
}