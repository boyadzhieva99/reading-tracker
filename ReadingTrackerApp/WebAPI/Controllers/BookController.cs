﻿namespace WebAPI.Controllers
{
    using ApplicationServices.DTOs;
    using ApplicationServices.Services;
    using System.Linq;
    using System.Web.Http;
    using WebAPI.Messages;

    [Authorize(Roles = "Admin")]
    [RoutePrefix("api/Book")]
    public class BookController : ApiController
    {
        private readonly BookManagementService _service = null;

        public BookController()
        {
            _service = new BookManagementService();
        }

        [OverrideAuthorization]
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Json(_service.Get());
        }

        [OverrideAuthorization]
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            return Json(_service.GetById(id));
        }

        [OverrideAuthorization]
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetByTitle(string title)
        {
            return Json(_service.GetByTitle(title));
        }

        [HttpPost]
        public IHttpActionResult Create([FromBody]BookDto bookDto)
        {
            if (!bookDto.Validate())
            {
                return Json(new ResponseMessage { Code = 500, Error = "Data is not valid!" });
            }

            ResponseMessage response = new ResponseMessage();

            if (_service.Save(bookDto))
            {
                response.Code = 200;
                response.Body = "Book is saved successfully!";
            }
            else
            {
                response.Code = 500;
                response.Body = "Book is not saved!";
            }

            return Json(response);
        }

        [HttpPut]
        public IHttpActionResult Update([FromBody] BookDto bookDto)
        {
            if (!bookDto.Validate())
            {
                return Json(new ResponseMessage { Code = 500, Error = "Data is not valid!" });
            }

            ResponseMessage response = new ResponseMessage();

            if (_service.Update(bookDto))
            {
                response.Code = 200;
                response.Body = "Book is updated successfully!";
            }
            else
            {
                response.Code = 500;
                response.Body = "Book is not updated!";
            }

            return Json(response);
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            ResponseMessage response = new ResponseMessage();

            if (_service.Delete(id))
            {
                response.Code = 200;
                response.Body = "Book is deleted!";
            }
            else
            {
                response.Code = 500;
                response.Body = "Book was not deleted!";
            }

            return Json(response);
        }

        //[Route("id/authors")]
        //[HttpPut]
        //public IHttpActionResult AddAuthorsToBook(int id, string[] authorToAdd)
        //{
        //    AuthorManagementService _authorService = new AuthorManagementService();

        //    if (authorToAdd == null)
        //    {
        //        return this.BadRequest("No authors to assign!");
        //    }

        //    //find the book to which we want to assign authors
        //    var book = _service.GetById(id);

        //    if(book == null)
        //    {
        //        return NotFound();
        //    }
        //}
    }
}