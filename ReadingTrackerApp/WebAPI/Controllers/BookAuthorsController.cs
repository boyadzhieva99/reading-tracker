﻿namespace WebAPI.Controllers
{
    using ApplicationServices.DTOs;
    using ApplicationServices.Services;
    using System.Web.Http;
    using WebAPI.Messages;

    [Authorize(Roles = "Admin")]
    [RoutePrefix("api/BookAuthors")]
    public class BookAuthorsController : ApiController
    {
        private readonly BookAuthorsManagementService _service = new BookAuthorsManagementService();

        public BookAuthorsController()
        {
            _service = new BookAuthorsManagementService();
        }

        [OverrideAuthorization]
        [AllowAnonymous]
        [HttpGet]
        //[Route("id")]
        public IHttpActionResult Get(int id)
        {
            return Json(_service.GetAuthorsByBookId(id));
        }

        [HttpPost]
        public IHttpActionResult AddAuthorsToBooks([FromBody] BookAuthorsDto dto)
        {
            ResponseMessage response = new ResponseMessage();

            if (_service.AddAuthors(dto))
            {
                response.Code = 200;
                response.Body = "Author is added to the book successfully!";
            }
            else
            {
                response.Code = 500;
                response.Body = "Author is not added to the book!";
            }

            return Json(response);
        }
    }
}
