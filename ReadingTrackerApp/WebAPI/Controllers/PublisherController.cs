﻿namespace WebAPI.Controllers
{
    using ApplicationServices.Services;
    using ApplicationServices.DTOs;
    using System.Web.Http;
    using WebAPI.Messages;

    [Authorize(Roles = "Admin")]
    [RoutePrefix("api/Publisher")]
    public class PublisherController : ApiController
    {
        private readonly PublisherManagementService _service = null;

        public PublisherController()
        {
            _service = new PublisherManagementService();
        }

        [OverrideAuthorization]
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Json(_service.Get());
        }

        [OverrideAuthorization]
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            return Json(_service.GetById(id));
        }

        [HttpPost]
        public IHttpActionResult Create([FromBody]PublisherDto publisherDto)
        {
            if (!publisherDto.Validate())
            {
                return Json(new ResponseMessage { Code = 500, Error = "Data is not valid!" });
            }

            ResponseMessage response = new ResponseMessage();

            if (_service.Save(publisherDto))
            {
                response.Code = 200;
                response.Body = "Publisher is saved successfully!";
            }
            else
            {
                response.Code = 500;
                response.Body = "Publisher is not saved!";
            }

            return Json(response);
        }

        [HttpPut]
        public IHttpActionResult Update([FromBody] PublisherDto publisherDto)
        {
            if (!publisherDto.Validate())
            {
                return Json(new ResponseMessage { Code = 500, Error = "Data is not valid!" });
            }

            ResponseMessage response = new ResponseMessage();

            if (_service.Update(publisherDto))
            {
                response.Code = 200;
                response.Body = "Publisher is updated successfully!";
            }
            else
            {
                response.Code = 500;
                response.Body = "Publisher is not updated!";
            }

            return Json(response);
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            ResponseMessage response = new ResponseMessage();

            if (_service.Delete(id))
            {
                response.Code = 200;
                response.Body = "Publisher is deleted successfully!";
            }
            else
            {
                response.Code = 500;
                response.Body = "Publisher was not deleted!";
            }

            return Json(response);
        }
    }
}