﻿namespace WebAPI.Controllers
{
    using ApplicationServices.DTOs;
    using ApplicationServices.Services;
    using System.Web.Http;
    using WebAPI.Messages;

    [Authorize(Roles = "Admin")]
    [RoutePrefix("api/Author")]
    public class AuthorController : ApiController
    {
        private readonly AuthorManagementService _service = null;

        public AuthorController()
        {
            _service = new AuthorManagementService();
        }

        [OverrideAuthorization]
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Json(_service.Get());
        }

        [OverrideAuthorization]
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            return Json(_service.GetById(id));
        }

        [HttpPost]
        public IHttpActionResult Create(AuthorDto authorDto)
        {
            if (!authorDto.Validate())
            {
                return Json(new ResponseMessage { Code = 500, Error = "Data is not valid!" });
            }

            ResponseMessage response = new ResponseMessage();

            if (_service.Save(authorDto))
            {
                response.Code = 200;
                response.Body = "Author is saved successfully!";
            }
            else
            {
                response.Code = 500;
                response.Body = "Author is not saved!";
            }

            return Json(response);
        }

        [HttpPut]
        public IHttpActionResult Update([FromBody] AuthorDto authorDto)
        {
            if (!authorDto.Validate())
            {
                return Json(new ResponseMessage { Code = 500, Error = "Data is not valid!" });
            }

            ResponseMessage response = new ResponseMessage();

            if (_service.Update(authorDto))
            {
                response.Code = 200;
                response.Body = "Author is updated successfully!";
            }
            else
            {
                response.Code = 500;
                response.Body = "Author is not updated!";
            }

            return Json(response);
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            ResponseMessage response = new ResponseMessage();

            if (_service.Delete(id))
            {
                response.Code = 200;
                response.Body = "Author is deleted!";
            }
            else
            {
                response.Code = 500;
                response.Body = "Author was not deleted!";
            }

            return Json(response);
        }
    }
}