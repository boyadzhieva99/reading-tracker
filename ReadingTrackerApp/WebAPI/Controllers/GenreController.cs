﻿namespace WebAPI.Controllers
{
    using ApplicationServices.Services;
    using ApplicationServices.DTOs;
    using System.Web.Http;
    using WebAPI.Messages;

    [Authorize(Roles = "Admin")]
    [RoutePrefix("api/Genre")]
    public class GenreController : ApiController
    {
        private readonly GenreManagementService _service = null;

        public GenreController()
        {
            _service = new GenreManagementService();
        }

        [OverrideAuthorization]
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Json(_service.Get());
        }

        [OverrideAuthorization]
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            return Json(_service.GetById(id));
        }

        [HttpPost]
        public IHttpActionResult Create([FromBody]GenreDto genreDto)
        {
            if (!genreDto.Validate())
            {
                return Json(new ResponseMessage { Code = 500, Error = "Data is not valid!" });
            }

            ResponseMessage response = new ResponseMessage();

            if (_service.Save(genreDto))
            {
                response.Code = 200;
                response.Body = "Genre is saved successfully!";
            }
            else
            {
                response.Code = 500;
                response.Body = "Genre is not saved!";
            }

            return Json(response);
        }

        [HttpPut]
        public IHttpActionResult Update([FromBody] GenreDto genreDto)
        {
            if (!genreDto.Validate())
            {
                return Json(new ResponseMessage { Code = 500, Error = "Data is not valid!" });
            }

            ResponseMessage response = new ResponseMessage();

            if (_service.Update(genreDto))
            {
                response.Code = 200;
                response.Body = "Genre is updated successfully!";
            }
            else
            {
                response.Code = 500;
                response.Body = "Genre is not updated!";
            }

            return Json(response);
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            ResponseMessage response = new ResponseMessage();

            if (_service.Delete(id))
            {
                response.Code = 200;
                response.Body = "Genre is deleted successfully!";
            }
            else
            {
                response.Code = 500;
                response.Body = "Genre was not deleted!";
            }

            return Json(response);
        }
    }
}