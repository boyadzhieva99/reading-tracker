﻿namespace WebAPI.BooksToUser
{
    using ApplicationData.Entities;
    using ApplicationServices.DTOs;

    public class UserBooksDto : IValidate
    {
        public string UserId { get; set; }
        public int BookId { get; set; }
        public BookStaus Status { get; set; }
        public BookFormat Format { get; set; }
        public BookRating Rating { get; set; }
        public string Review { get; set; }

        public bool Validate()
        {
            return Review.Length <= 6000;
        }
    }
}