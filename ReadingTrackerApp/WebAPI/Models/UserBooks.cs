﻿namespace WebAPI.Models
{
    using ApplicationData.Entities;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class UserBooks
    {
        [Key]
        [Column(Order = 1)]
        public string ApplicationUserId { get; set; }
        [ForeignKey("ApplicationUserId")]
        public ApplicationUser ApplicationUser { get; set; }

        [Key]
        [Column(Order = 2)]
        public int BookId { get; set; }
        [ForeignKey("BookId")]
        public Book Book { get; set; }

        public BookStaus Status { get; set; }
        public BookFormat Format { get; set; }
        public BookRating Rating { get; set; }

        [StringLength(6000)]
        public string Review { get; set; }
    }
}
