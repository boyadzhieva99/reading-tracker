﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using ApplicationData.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace WebAPI.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        //property used for soft deleting users
        public bool IsDeleted { get; set; }

        public virtual ICollection<Book> Books { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager,
            string authenticationType)
        {
            // Note the authenticationType must match the one defined 
            //in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class AppUsersDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<UserBooks> UserBooks { get; set; }

        public AppUsersDbContext()
            : base("AppUsers", throwIfV1Schema: false)
        {
        }

        public static AppUsersDbContext Create()
        {
            return new AppUsersDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserBooks>()
                        .HasKey(ub => new { ub.ApplicationUserId, ub.BookId });

            //modelBuilder.Entity<Book>()
            //    .HasMany<ApplicationUser>(b => b.Users) // book entity has many users
            //    .WithMany(u => u.Books); // User entity includes many book entities

            base.OnModelCreating(modelBuilder);
        }
    }
}