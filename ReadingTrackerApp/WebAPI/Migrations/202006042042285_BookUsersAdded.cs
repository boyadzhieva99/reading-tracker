﻿namespace WebAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BookUsersAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserBooks",
                c => new
                    {
                        ApplicationUserId = c.String(nullable: false, maxLength: 128),
                        BookId = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        Format = c.Int(nullable: false),
                        Rating = c.Int(nullable: false),
                        Review = c.String(),
                    })
                .PrimaryKey(t => new { t.ApplicationUserId, t.BookId })
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId, cascadeDelete: true)
                .ForeignKey("dbo.Books", t => t.BookId, cascadeDelete: true)
                .Index(t => t.ApplicationUserId)
                .Index(t => t.BookId);
            
            //CreateTable(
            //    "dbo.Books",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            ISBN = c.Long(nullable: false),
            //            Title = c.String(nullable: false, maxLength: 100),
            //            PublicationDate = c.DateTime(),
            //            GenreId = c.Int(nullable: false),
            //            PublisherId = c.Int(nullable: false),
            //            Price = c.Decimal(nullable: false, precision: 18, scale: 2),
            //            Description = c.String(),
            //            ImageUrl = c.String(maxLength: 2000),
            //            CreatedOn = c.DateTime(),
            //            UpdatedOn = c.DateTime(),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Genres", t => t.GenreId, cascadeDelete: true)
            //    .ForeignKey("dbo.Publishers", t => t.PublisherId, cascadeDelete: true)
            //    .Index(t => t.GenreId)
            //    .Index(t => t.PublisherId);
            
            //CreateTable(
            //    "dbo.Authors",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            FName = c.String(nullable: false, maxLength: 25),
            //            LName = c.String(nullable: false, maxLength: 25),
            //            DateOfBirth = c.DateTime(),
            //            Age = c.Byte(nullable: false),
            //            Gender = c.Int(nullable: false),
            //            Nationality = c.String(maxLength: 50),
            //            CreatedOn = c.DateTime(),
            //            UpdatedOn = c.DateTime(),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.Genres",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Name = c.String(nullable: false, maxLength: 25),
            //            CreatedOn = c.DateTime(),
            //            UpdatedOn = c.DateTime(),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.Publishers",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Name = c.String(nullable: false, maxLength: 25),
            //            CEO = c.String(maxLength: 50),
            //            Country = c.String(nullable: false, maxLength: 50),
            //            Address = c.String(maxLength: 100),
            //            YearsActive = c.Byte(nullable: false),
            //            CreatedOn = c.DateTime(),
            //            UpdatedOn = c.DateTime(),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.AuthorBooks",
            //    c => new
            //        {
            //            Author_Id = c.Int(nullable: false),
            //            Book_Id = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => new { t.Author_Id, t.Book_Id })
            //    .ForeignKey("dbo.Authors", t => t.Author_Id, cascadeDelete: true)
            //    .ForeignKey("dbo.Books", t => t.Book_Id, cascadeDelete: true)
            //    .Index(t => t.Author_Id)
            //    .Index(t => t.Book_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserBooks", "BookId", "dbo.Books");
            DropForeignKey("dbo.Books", "PublisherId", "dbo.Publishers");
            DropForeignKey("dbo.Books", "GenreId", "dbo.Genres");
            DropForeignKey("dbo.AuthorBooks", "Book_Id", "dbo.Books");
            DropForeignKey("dbo.AuthorBooks", "Author_Id", "dbo.Authors");
            DropForeignKey("dbo.UserBooks", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.AuthorBooks", new[] { "Book_Id" });
            DropIndex("dbo.AuthorBooks", new[] { "Author_Id" });
            DropIndex("dbo.Books", new[] { "PublisherId" });
            DropIndex("dbo.Books", new[] { "GenreId" });
            DropIndex("dbo.UserBooks", new[] { "BookId" });
            DropIndex("dbo.UserBooks", new[] { "ApplicationUserId" });
            DropTable("dbo.AuthorBooks");
            DropTable("dbo.Publishers");
            DropTable("dbo.Genres");
            DropTable("dbo.Authors");
            DropTable("dbo.Books");
            DropTable("dbo.UserBooks");
        }
    }
}
